import numpy as np
import pygame as pg
import constants

def init():
    pg.init()
    screen = pg.display.set_mode(size=(constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT))
    time = pg.time.Clock()

    return screen, time


# Init game
screen, time = init()


def fill_matrix():
    return np.array([[0 for i in range(constants.GRID_WIDTH)] for j in range(constants.GRID_HEIGHT)])


def initialize_matrix():
    return np.array([[np.random.randint(2) for i in range(constants.GRID_WIDTH)] for j in range(constants.GRID_HEIGHT)])


def get_count_alive_cells(current_field):
    count = 0

    for j in range(len(current_field[:])):
        for i in range(len(current_field[0])):
            if current_field[j][i] == 1:
                count += 1

    return count


def get_state_cell(current_field, x, y):
    count = 0
    for j in range(y - 1, y + 2):
        for i in range(x - 1, x + 2):
            if current_field[j][i]:
                count += 1

    if current_field[y][x]:
        count -= 1
        if count == 2 or count == 3:
            return 1
        return 0
    else:
        if count == 3:
            return 1
        return 0


def draw_grid():
    for x in range(0, constants.SCREEN_WIDTH, constants.CELL_SIZE):
        pg.draw.line(
            screen,
            pg.Color(constants.GRID_COLOR), 
            (x, 0), 
            (x, constants.SCREEN_WIDTH)
        )

    for y in range(0, constants.SCREEN_HEIGHT, constants.CELL_SIZE):
        pg.draw.line(
            screen,
            pg.Color(constants.GRID_COLOR),
            (0, y),
            (constants.SCREEN_HEIGHT, y)
        )


def draw_cells(current_field):
    for x in range(1, constants.GRID_WIDTH - 1):
        for y in range(0, constants.GRID_HEIGHT - 1):
            if current_field[y][x] == 1:
                pg.draw.rect(
                    screen,
                    pg.Color(constants.CELL_COLOR_BLACK),
                    (
                        x * constants.CELL_SIZE + 2,
                        y * constants.CELL_SIZE + 2,
                        constants.CELL_SIZE - 2,
                        constants.CELL_SIZE - 2
                    )
                )


def compute_matrix(current_field):
    for x in range(1, constants.GRID_WIDTH - 1):
        for y in range(0, constants.GRID_HEIGHT - 1):
            next_field[y][x] = get_state_cell(current_field, x, y)

    return np.copy(next_field)


def run_matrix(current_field, iterations):
    updated_field = compute_matrix(current_field)

    if iterations > 0:
        return run_matrix(updated_field, iterations - 1)

    return updated_field
    

# Initialize field of cells
next_field = fill_matrix()
current_field = initialize_matrix()


# Draw each frame
while True:
    screen.fill(pg.Color(constants.BACKGROUND_COLOR))

    for event in pg.event.get():
        if event.type == pg.QUIT:
            exit()

    draw_grid()
    current_field = compute_matrix(current_field)
    draw_cells(current_field)

    pg.display.flip()
    time.tick(constants.FRAME_RATE)
  

