# Game of Life

## Developers: Evgeniy Voronov, Hanwen LIU, Andrea Chahwan 

The game of life is a cellular automaton that was invented by John Conway in 1970. The game consists of a collection of cells that die, live or multiply depending on some given rules. In this project, we model our space as a matrix, each cell of the matrix is either alive or dead. The matrix
is a torus, that is, each cell has 8 neighboring cells. For instance, cell O of coordinate s (x = 0, y =4) in Figure 1 has the following neighboring cells:

### • Cell A of coordinate (x = 4, y = 4)
### • Cell B of coordinate (x = 0, y = 0)
### • Cell C of coordinate (x = 1, y = 4) 
### • Cell D of coordinate (x = 0, y = 3) 
### • Cell E of coordinate (x = 4, y = 0) 
### • Cell F of coordinate (x = 1, y = 0) 
### • Cell H of coordinate (x = 1, y = 3) 
### • Cell I of coordinate (x = 4, y = 3)
 
Given a matrix in a given state (some cells are alive where some others are dead), the objective is to compute the state of the matrix after applying the life rules. These rules are described in the following:
### • Living rule: Each living cell that has 2 or 3 neighboring living cells remains alive.
### • Death rule: Each living cell that has at least 4 neighboring living cells dies of overpopulation. Similarly, each living cell that has 1 or 0 neighboring living cell dies of isolation.
### • Birth Rule: Each dead cell that has exactly three neighboring living cells becomes alive.
An example is presented in the following. The first matrix represents the initial matrix that we will
call M. The second matrix represents the state of matrix M after applying the above mentioned rules.

## Part 1
We will use arrays of two dimensions to represent a matrix. A living cell is represented by 1 while
a dead cell is represented by 0. We will also use an array of positions (x,y) to that specifies the positions of the living cells inside the matrix.
1. Write a python function fill_matrix that initializes a given matrix using the position array.

2. Write a python function initialize_matrix that initializes a given matrix given a rate of living cells. For instance, if the given parameter is equal to 10 and the size of the matrix is 20 then the matrix has around 40 cells alive. The positions of the living cells are chosen randomly.

3. Write a python function count_cells that counts the number of cells that are alive in a given matrix.

4. Write a python function state_cell that given a matrix and a cell returns the state of the cell after applying the life game rules.

5. Write a python function compute_matrix that given a matrix, returns the state of the matrix after applying the life game rules to all the cells.

6. Write a python function run_matrix that given a matrix M and a number of iterations i returns M matrix after the i iterations (after applying the game rules i times).

7. Write a program to test all the above functions.


## Part 2
The objective of the second part of the project is to simulate a garden with cabbage. The setting is
similar to the one of part 1. The matrix represents the garden; each cell of the matrix contains possibly a cabbage. This time, the cabbage has more properties, as explained in the following: 

1. Each cabbage has an energy that increases by 4 at each iteration of the simulation.

2. Each cabbage has an age that increases by 1 at each iteration of the simulation. • Each cabbage has a position in the garden (the position in the matrix).

The rules in this part of the projects are the following:

    1. Each cabbage of age=5 dies immediately.
    
    2. Each cabbage cell of age less than 5 with an energy greater or equal to 10, tries to duplicate itself in the neighboring cells (every dead neighboring cell becomes alive). This duplication uses 10 units from the cell energy. When a cabbage is created, its energy is equal to 1.

When we display a garden every cell is either equal to C to say that it contains a Cabbage or equal to – to say that the cell is dead.

3. Write a python function initialize to initialize a garden randomly.

4. Write a python function display to display the state of the garden.

5. Write a python function run that returns the state of the garden after some given number
of iterations i.


## Part 3
Let consider the garden described in part 2 and let add to our garden goats. As for the cabbage, the goat has an age, an energy and a position in the garden. At each iteration, the age of the goat is incremented by 1. The living rules for the goats are the following:

    1. If the goat has either an age equal to 15 or an energy less or equal to 0 it dies immediately.

    2. If the goat is on a cell of the garden that contains a Cabbage, its energy increases by 14 while the cabbage dies as it is eaten by the goat.

    3. If the energy of the goat is larger or equal to 20, the goat duplicate itself on one of the adjacent cells that does not contain a goat. This reproduction takes 20 units from the energy of the goat. The new born goat has an initial energy equal to 11.

    4. The goat can move to an adjacent cell if the cell does not contain another goat. The goat always chooses to move to a neighboring cell with a cabbage. This action takes 2 units
    from the goat energy.

    5. If none of the above actions is available, the goat does nothing. Each cell is displayed two characters:

    6. -- : the cell is free.

    7. -g: the cell contains a goat.

    8. c-: the cell contains a cabbage.

    9. cg: the cell contains a cabbage and a goat.

1. Write a python function initialize to initialize a garden randomly.

2. Write a python function display to display the state of the garden.

3. Write a python function run that returns the state of the garden after some given number of iterations i.
