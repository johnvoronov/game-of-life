import numpy as np
import pygame as pg
import constants


TYPE_CELL_CABBAGE = 1
START_AGE_CABBAGE = 0
START_ENERGY_CABBAGE = 1
END_AGE_CABBAGE = 5
INCREASE_AGE_CABBAGE = 1
INCREASE_ENERGY_CABBAGE = 4
REPLICATION_ENERGY_CABBAGE = 10

TYPE_CELL_GOAT = 2
START_AGE_GOAT = 0
START_ENERGY_GOAT = 11
INCREASE_AGE_GOAT = 1
INCREASE_ENERGY_GOAT = 14
DECREASE_ENERGY_GOAT = 2
END_AGE_GOAT = 15
END_ENERGY_GOAT = 0
REPLICATION_ENERGY_GOAT = 20

def init():
    pg.init()
    screen = pg.display.set_mode(size=(constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT))
    time = pg.time.Clock()

    return screen, time


# Init game
screen, time = init()

def fill_matrix_garden():
    return np.array([[[0, 0, 0, 0] for i in range(constants.GRID_WIDTH)] for j in range(constants.GRID_HEIGHT)])

def initialize_matrix_cabbage():
    matrix = fill_matrix_garden()

    for j in range(constants.GRID_WIDTH):
        for i in range(constants.GRID_HEIGHT):
            state = np.random.randint(2)
            age = 0
            energy = 0
            
            if state == 1:
                age = START_AGE_CABBAGE
                energy = START_ENERGY_CABBAGE

                matrix[j][i] = [state, age, energy, TYPE_CELL_CABBAGE]
    
    return matrix

def initialize_matrix_goat():
    matrix = fill_matrix_garden()

    for j in range(constants.GRID_WIDTH):
        for i in range(constants.GRID_HEIGHT):
            state = np.random.randint(2)
            age = START_AGE_GOAT
            energy = START_ENERGY_CABBAGE
            
            if state == 1:
                age = START_AGE_GOAT
                energy = START_ENERGY_CABBAGE

                matrix[j][i] = [state, age, energy, TYPE_CELL_GOAT]

    return matrix


def get_count_alive_cells(current_field):
    count = 0

    for j in range(len(current_field[:])):
        for i in range(len(current_field[0])):
            if current_field[j][i][0] == 1:
                count += 1

    return count


def get_state_cell(current_field, x, y):
    if current_field[y][x][0]:
        if current_field[y][x][3] == TYPE_CELL_CABBAGE: 
            if current_field[y][x][1] < END_AGE_CABBAGE:
                # Add a life
                current_field[y][x][1] += INCREASE_AGE_CABBAGE

                if current_field[y][x][2] < REPLICATION_ENERGY_CABBAGE:
                    # Add a energy
                    current_field[y][x][2] += INCREASE_ENERGY_CABBAGE

                elif current_field[y][x][2] >= REPLICATION_ENERGY_CABBAGE:
                    current_field[y][x] = [0, 0, 0, 0]
                    
                    # Create new cabbege
                    for j in range(y - 1, y + 2):
                        for i in range(x - 1, x + 2):
                            if j != y and i != x:
                                if current_field[j][i][0] == 0:
                                    current_field[j][i] = [1, START_AGE_CABBAGE, START_ENERGY_CABBAGE, TYPE_CELL_CABBAGE]

            elif current_field[y][x][1] == END_AGE_CABBAGE:
                current_field[y][x] = [0, 0, 0, 0]

            return current_field[y][x]

        elif current_field[y][x][3] == TYPE_CELL_GOAT:
            if current_field[y][x][1] < END_AGE_GOAT:
                # Add a life
                current_field[y][x][1] += INCREASE_AGE_GOAT
                
                if current_field[y][x][2] >= REPLICATION_ENERGY_GOAT:

                    # Create new goat
                    for j in range(y - 1, y + 2):
                        for i in range(x - 1, x + 2):
                            if j != y and i != x:
                                if current_field[j][i][0] == 0 or current_field[j][i][3] == TYPE_CELL_GOAT:
                                    current_field[j][i] = [1, START_AGE_GOAT, START_ENERGY_GOAT, TYPE_CELL_GOAT]
                                    break

                # Goat movement
                next_goat_position_x = 0
                next_goat_position_y = 0

                for j in range(y - 1, y + 2):
                    for i in range(x - 1, x + 2):
                        if j != y and i != x:
                            if current_field[j][i][3] == TYPE_CELL_CABBAGE:
                                next_goat_position_y = j
                                next_goat_position_x = i
                                break
                
                if next_goat_position_y != 0 and next_goat_position_x != 0:
                    current_field[y][x][2] += INCREASE_ENERGY_GOAT
                    current_field[y][x][2] -= DECREASE_ENERGY_GOAT
                    current_field[next_goat_position_y][next_goat_position_x] = current_field[y][x]
                    current_field[y][x] = [0, 0, 0, 0]

            elif current_field[y][x][1] == END_AGE_GOAT:
                current_field[y][x] = [0, 0, 0, 0]

            return current_field[y][x]
    else:
        return [0, 0, 0, 0]


def draw_grid():
    for x in range(0, constants.SCREEN_WIDTH, constants.CELL_SIZE):
        pg.draw.line(
            screen,
            pg.Color(constants.GRID_COLOR), 
            (x, 0), 
            (x, constants.SCREEN_WIDTH)
        )

    for y in range(0, constants.SCREEN_HEIGHT, constants.CELL_SIZE):
        pg.draw.line(
            screen,
            pg.Color(constants.GRID_COLOR),
            (0, y),
            (constants.SCREEN_HEIGHT, y)
        )


def draw_cells(current_field):
    for x in range(1, constants.GRID_WIDTH - 1):
        for y in range(0, constants.GRID_HEIGHT - 1):
            if current_field[y][x][0] == 1:
                color_cell = constants.CELL_COLOR_GREEN

                if current_field[y][x][3] == TYPE_CELL_GOAT:
                    color_cell = constants.CELL_COLOR_BLACK

                pg.draw.rect(
                    screen,
                    pg.Color(color_cell),
                    (
                        x * constants.CELL_SIZE + 2,
                        y * constants.CELL_SIZE + 2,
                        constants.CELL_SIZE - 2,
                        constants.CELL_SIZE - 2
                    )
                )


def compute_matrix(current_field):
    for x in range(1, constants.GRID_WIDTH - 1):
        for y in range(0, constants.GRID_HEIGHT - 1):
            next_field[y][x] = get_state_cell(current_field, x, y)

    return np.copy(next_field)


def run_matrix(current_field, iterations):
    updated_field = compute_matrix(current_field)

    if iterations > 0:
        return run_matrix(updated_field, iterations - 1)

    return updated_field

def overlay_matrix_to_matrix(current_field_cabbage, current_field_goat):
    for x in range(1, constants.GRID_WIDTH - 1):
        for y in range(0, constants.GRID_HEIGHT - 1):
            if current_field_cabbage[y][x][0] and current_field_goat[y][x][0]:
                current_field_goat[y][x] = [1, START_AGE_GOAT, START_ENERGY_GOAT, TYPE_CELL_GOAT]
                current_field_cabbage[y][x] = current_field_goat[y][x]

    return current_field_cabbage


# Initialize field of cells
next_field = fill_matrix_garden()
current_field_cabbage = initialize_matrix_cabbage()
current_field_goat = initialize_matrix_goat()
current_field = overlay_matrix_to_matrix(current_field_cabbage, current_field_goat)


# Draw each frame
while True:
    screen.fill(pg.Color(constants.BACKGROUND_COLOR))

    for event in pg.event.get():
        if event.type == pg.QUIT:
            exit()

    draw_grid()
    current_field = compute_matrix(current_field)
    draw_cells(current_field)

    pg.display.flip()
    time.tick(constants.FRAME_RATE)
  

