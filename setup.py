import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Game of Life",
    version="1.0.0",
    author="Evgenii, Hanwen, Andrea",
    author_email="johnvoronov@gmail.com",
    description="The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970. It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input.",
    url="https://github.com/johnvoronov/game-of-life",
    packages=setuptools.find_packages(),
    install_requires=['pygame'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)